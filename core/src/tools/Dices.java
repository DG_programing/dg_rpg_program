package tools;

import java.util.Locale;
import java.util.Random;

public class Dices {
	private static final int[] regularDices = {2, 4, 6, 8, 10, 12, 20, 100};
	private static int dice = 0;

	
/*	rollDice(int size):	Erstellt eine zufällig generierte zahl zwischen 1 & "size"							*
 * 			Verlinkt zu "checkRegularDice()" um den "size"-Wert auf gültigkeit zu überprüfen				*
 * 			Falls der "size"-Wert ungültig ist, wird zu "printWrongDice()" verlinkt.						*/
	
	public static int rollDice(int size) {
		if (checkRegularDice(size) == true) {
			Random random = new Random();
			dice = random.nextInt(size) + 1;	
			return dice;
		} else {
			printWrongDice();
			return 0;
		}	
	} // end of rollDice(int size)

	
/*	rollDice(int size, int quantity):	Wie bei rollDice(int size), jedoch könne mehrere Würfel übergeben	*
 * 			werden. Es wird der Gesamtwert aller Würfel zurückgegeben										*/
	
	public static int rollDice(int size, int quantity) {
		if (checkRegularDice(size) == true) {
			for (int i = 0; i < quantity; i++) {
				Random random = new Random();
				dice = dice + random.nextInt(size) + 1;	
			}
			return dice;
		} else {
			printWrongDice();
			return 0;
		}		
	} // end of rollDice(int size, int quantity)
	
	
/*	checkRegularDice:	Überprüft den übergebenen Wert "size", ob dieser eine reguläre Würfelgröße hat		*
 * 			Der Wert "size" wird mit den Werten aus dem Array "regularDices" veglichen.						*
 * 			Falls ein Wert passt wird ein true zurückgegeben, andernfalls ein false.						*/
	
	private static boolean checkRegularDice(int size) {
		for (int i = 0; i < regularDices.length; i++) {
			if (regularDices[i] != size) {
				if (i == regularDices.length && regularDices[i] != size) {
					return false;
				} else {
					continue;
				}
			} else {
				return true;
			}
		}
		return false;
	} // end of checkRegularDice(int size)
	
	
/*	printWrongSize:	Fehlerausdruck falls "size" keine reguläre Würfelgröße									*
 * 			Überprüft die Sprache des Systems für Ausgabe in Deutsch oder Englisch							*/
	
	public static void printWrongDice() {
		if (Locale.getDefault().getLanguage() == Locale.GERMAN.getLanguage()) {
			System.out.println("Bitte einen regulären Würfel wählen: ");
			for (int i = 0; i < regularDices.length; i++) {
				System.out.print("W" + regularDices[i]);
				if (i != regularDices.length-1) {
					System.out.print(", ");
				}
			}
		} else {
			System.out.println("Please choose a regular Dice: ");
			for (int i = 0; i < regularDices.length; i++) {
				System.out.print("d" + regularDices[i]);
				if (i != regularDices.length-1) {
					System.out.print(", ");
				}
			}
		}
	} // end of printWrong Dice()
}
